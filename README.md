#gopatcher

    ~/go/src/git.oschina.net/kayj/gopatcher $ gopatcher help
    Usage: gopatcher modtime [exclude extensions] [output dir]
    Examples: gopatcher 2014-04-23T15:21:37
              gopatcher 2014-04-23T15:21:37 jpg,doc
              gopatcher 2014-04-23T15:21:37 jpg,doc /Users/kayj/Desktop

把当前目录下的满足条件的文件打包：

- `gopatcher 2014-04-23T15:21:37` 
    - 文件修改日期在2014-04-23 15:21:37之后。
    - 打包到用户主目录下

- `gopatcher 2014-04-23T15:21:37 jpg,doc` 
    - 文件修改日期在2014-04-23 15:21:37之后。
    - 文件名不以jpg、doc结尾。
    - 打包到用户主目录下。

- `gopatcher 2014-04-23T15:21:37 jpg,doc /Users/kayj/Desktop` 
    - 文件修改日期在2014-04-23 15:21:37之后。
    - 文件名不以jpg、doc结尾。
    - 打包到/Users/kayj/Desktop目录下。